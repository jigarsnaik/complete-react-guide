import './App.css';
import ExpenseItem from "./components/ExpenseItem";

function App() {
    return (
        <div className="App">
            Let's get started
            <ExpenseItem></ExpenseItem>
        </div>
    );
}

export default App;
